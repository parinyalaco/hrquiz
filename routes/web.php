<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuizSetsController;
use App\Http\Controllers\QuestionTypesController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\PersonsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('quiz-sets/startquiz/{id}', [QuizSetsController::class, 'startquiz']);
Route::post('quiz-sets/startquizAction/{id}', [QuizSetsController::class, 'startquizAction']);
Route::get('quiz-sets/runquiz/{id}/{person_id}/{token}/{question_id}', [QuizSetsController::class, 'runquiz']);
Route::post('quiz-sets/runquizAction/{id}/{person_id}/{token}/{question_id}', [QuizSetsController::class, 'runquizAction']);
Route::get('quiz-sets/preparequiz/{id}/{person_id}/{token}', [QuizSetsController::class, 'preparequiz']);
Route::get('quiz-sets/endquiz/{id}/{person_id}/{token}', [QuizSetsController::class, 'endquiz']);

Route::resource('quiz-sets', QuizSetsController::class)->middleware('auth');
Route::resource('question-types', QuestionTypesController::class)->middleware('auth');
Route::resource('questions', QuestionsController::class)->middleware('auth');
Route::resource('persons', PersonsController::class)->middleware('auth');


Route::get('/', [App\Http\Controllers\QuizSetsController::class, 'index'])->middleware('auth');

Auth::routes();
//Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/importView/{id}', [QuestionTypesController::class, 'importExportView'])->name('quiz_importView');
Route::post('/quiz/import/{id}', [QuestionTypesController::class, 'import'])->name('quiz_import');
