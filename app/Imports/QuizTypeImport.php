<?php

namespace App\Imports;

use App\Models\QuestionType;
use Maatwebsite\Excel\Concerns\ToModel;

class QuizTypeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new QuestionType([
            'name'     => $row['name'],
        ]);
    }
}
