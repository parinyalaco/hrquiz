<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    protected $fillable = ['people_id','question_id','choice_id','result','result_txt'];

    public function person()
    {
        return $this->hasOne('App\Models\Person', 'id', 'people_id');
    }

    public function question()
    {
        return $this->hasOne('App\Models\Question', 'id', 'question_id');
    }

    public function choice()
    {
        return $this->hasOne('App\Models\Choice', 'id', 'choice_id');
    }

}
