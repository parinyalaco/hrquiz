<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'email', 'mobile', 'quiz_set_id','current_question','token','status'];

    public function quizset()
    {
        return $this->hasOne('App\Models\QuizSet', 'id', 'quiz_set_id');
    }
    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'people_id');
    }
}
