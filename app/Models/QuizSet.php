<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizSet extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'desc', 'status','owner_id'];

    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'quiz_set_id');
    }
}
