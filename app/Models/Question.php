<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = ['quiz_set_id','question_type_id', 'seq','title','desc','image','duration','maxscrore','status'];

    public function quizset()
    {
        return $this->hasOne('App\Models\QuizSet', 'id', 'quiz_set_id');
    }

    public function questiontype()
    {
        return $this->hasOne('App\Models\QuestionType', 'id', 'question_type_id');
    }

    public function choices()
    {
        return $this->hasMany('App\Models\Choice', 'question_id');
    }
}
