<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\QuestionType;
use App\Models\QuizSet;
use App\Models\Question;
use App\Models\Choice;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Auth;

use App\Imports\QuizTypeImport;
use Carbon\Carbon;
use DB;
use SimpleXLSX;
use App\Imports\PeriodImport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class QuestionTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $questiontypes = QuestionType::latest()->paginate($perPage);
        } else {
            $questiontypes = QuestionType::latest()->paginate($perPage);
        }

        return view('question-types.index', compact('questiontypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('question-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        QuestionType::create($requestData);

        return redirect('question-types')->with('flash_message', 'QuestionType added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $questiontype = QuestionType::findOrFail($id);

        return view('question-types.show', compact('questiontype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $questiontype = QuestionType::findOrFail($id);

        return view('question-types.edit', compact('questiontype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $questiontype = QuestionType::findOrFail($id);
        $questiontype->update($requestData);

        return redirect('question-types')->with('flash_message', 'QuestionType updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        QuestionType::destroy($id);

        return redirect('question-types')->with('flash_message', 'QuestionType deleted!');
    }

    public function importExportView($id)
    {
       return view('question-types.import', compact('id'));
    }

    public function import(Request $request, $id) 
    {
        // dd($id);
        set_time_limit(0);
        if ($request->hasFile('file_upload')) {
            $tmpfolder = md5(time());
            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path('storage/upload/' . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = 'upload/' . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;
            // dd($uploadfile);
            $tmpUploadD = array();
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                foreach ($xlsx->rows() as $r => $row) {
                    $quiz_sets= array();
                    $questions= array();
                    // dd($row);
                    if ($r==0) {
                        $query_quiz = QuizSet::where('name', $row[1])->first();
                        if(!empty($query_quiz)){
                            return view('question-types.import')->with('error','ไม่สามารถ upload ได้ เนื่องจากชื่อชุดประเมินซ้ำกับที่มีอยู่ค่ะ');
                        }else{
                            $quiz_sets['name'] = $row[1]; 
                            $quiz_sets['desc'] = $row[2];                                                         
                            $quiz_sets['status'] = 'Active';
                            $quiz_sets['owner_id'] = Auth::user()->id;
                            $query_quizset = QuizSet::create($quiz_sets);
                            $data_quizset = $query_quizset->id;
                            $seq_quiz = 0;
                        }
                    }elseif ($r > 1) {
                        // $requestData = $request->all();                        
                        $main_quiz['question_type_id'] = $id;
                        $quiz_name = Str::of($row[1])->trim();
                        // dd($quiz_name);
                        $count_main = strlen($quiz_name);
                        if($count_main>0){
                            $seq_quiz = $seq_quiz+1;                                
                            $main_quiz['quiz_set_id'] = $data_quizset;                                
                            $main_quiz['seq'] = $seq_quiz;
                            $main_quiz['title'] = $row[1];
                            $main_quiz['desc'] = $row[2];
                            $main_quiz['maxscore'] = $row[3];
                            $main_quiz['duration'] = $row[4];                            
                            $main_quiz['status'] = 'Active';
                            // dd($main_quiz);
                            $query_quiz = Question::create($main_quiz);
                            // dd($query_mquiz);
                            $data_quiz = $query_quiz->id;                        
                            if($id==1){     //Choice ตัวเลือก                         
                                // $seq_sub = 0;
                                for($i=1; $i<=5; $i++){ 
                                    $row_id = 4+$i;
                                    $choice_name = Str::of($row[$row_id])->trim();
                                    // dd($quiz_name);
                                    $count_choice = strlen($choice_name);
                                    if($count_choice>0){                             
                                        $sub_quiz['question_id'] = $data_quiz;                                
                                        $sub_quiz['seq'] = $i;
                                        $sub_quiz['title'] = $row[$row_id];
                                        if($row[10]==$i){
                                            $sub_quiz['result'] = $row[3];
                                        }else{
                                            $sub_quiz['result'] = 0;
                                        }
                                        $sub_quiz['status'] = 'Active';
                                        // dd($sub_quiz);
                                        $query_squiz = Choice::create($sub_quiz); 
                                    } 
                                }                                                
                            }elseif($id==2 || $id==3){    //Text เขียนคำตอบ
                                $sub_quiz['question_id'] = $data_quiz;                                
                                $sub_quiz['seq'] = 1;
                                if($id==2){
                                    $sub_quiz['title'] = 'Text';
                                }else{
                                    $sub_quiz['title'] = 'Match';
                                }                                
                                $sub_quiz['desc'] = $row[5];
                                $sub_quiz['result'] = $row[3];
                                $sub_quiz['status'] = 'Active';
                                // dd($sub_quiz);
                                $query_squiz = Choice::create($sub_quiz);                        
                            // }elseif($id==3){    //Match ล็อคคำตอบ                        
                            }
                        }
                    }
                }
            } else {
                echo SimpleXLSX::parseError();
            }
            
            // Cache::flush();
            // return redirect('user')->with('success','User updated successfully');
            return back()->with('success','Updated successfully');
        }        
    }
}
