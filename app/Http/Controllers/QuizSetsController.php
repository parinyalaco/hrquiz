<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Answer;
use App\Models\QuizSet;
use App\Models\Person;
use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class QuizSetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $this->middleware('auth');

        $keyword = $request->get('search');
        $perPage = 25;

        // if (!empty($keyword)) {
        //     $quizsets = QuizSet::latest()->paginate($perPage);
        //     // $quizsets = QuizSet::join('users', 'quiz_sets.owner_id', '=', 'users.id')->select('')->latest()->paginate($perPage);
        // } else {
        //     $quizsets = QuizSet::latest()->paginate($perPage);
        // }
        $quizsets = QuizSet::join('users', 'quiz_sets.owner_id', '=', 'users.id', 'left outer')
                    ->select('quiz_sets.id' , 'quiz_sets.name' , 'quiz_sets.desc', 'quiz_sets.status', 'quiz_sets.owner_id', 'users.name AS user_name', 'users.group')
                    ->latest('quiz_sets.created_at')->paginate($perPage);
        return view('quiz-sets.index', compact('quizsets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->middleware('auth');
        return view('quiz-sets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->middleware('auth');
        $requestData = $request->all();
        
        QuizSet::create($requestData);

        return redirect('quiz-sets')->with('flash_message', 'QuizSet added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $this->middleware('auth');
        $quizset = QuizSet::findOrFail($id);

        return view('quiz-sets.show', compact('quizset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $this->middleware('auth');
        $quizset = QuizSet::findOrFail($id);
        $user_pluck = User::pluck('name', 'id');

        return view('quiz-sets.edit', compact('quizset','user_pluck'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->middleware('auth');
        $requestData = $request->all();
        
        $quizset = QuizSet::findOrFail($id);
        $quizset->update($requestData);

        return redirect('quiz-sets')->with('flash_message', 'QuizSet updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->middleware('auth');
        QuizSet::destroy($id);

        return redirect('quiz-sets')->with('flash_message', 'QuizSet deleted!');
    }

    public function startquiz($id)
    {
        $this->middleware('auth');
        $quizset = QuizSet::findOrFail($id);

        return view('quiz-sets.startquiz', compact('quizset'));
    }

    public function startquizAction(Request $request, $id)
    {
        $this->middleware('auth');

        $requestData = $request->all();

        $requestData['token'] = sha1(mt_rand(1, 90000) . 'SALT');
        $requestData['status'] = 'Ready';

        $person = Person::create($requestData);
       

        return redirect('persons' )->with('flash_message', 'QuizSet updated!');
    }

    public function runquiz($id,$person_id,$token,$question_id){
        $quizset = QuizSet::findOrFail($id);
        $person = Person::where('id', $person_id)->where('token', $token)->first();
        $question = array();
        if($question_id == 'first'){
            if($quizset->questions->count() > 0){
                $question =  $quizset->questions[0];
            }
        }else{
            $question = Question::findOrFail($question_id);
        }

        //check refreash
        if($person->current_question == $question->id){
            $question =  $quizset->questions()->where('id','>', $question->id)->first();
            if(empty($question)){
                return redirect('quiz-sets/endquiz/' . $id . '/' . $person->id . '/' . $person->token )->with('flash_message', 'QuizSet End!');
            }else{
                return redirect('quiz-sets/runquiz/' . $id . '/' . $person->id . '/' . $person->token . '/' . $question->id)->with('flash_message', 'QuizSet updated!');
            }
            
        }else{
            $havequiz = Answer::where('people_id', $person_id)->where('question_id', $question->id)->count();
            if($havequiz > 0){
                $question =  $quizset->questions()->where('id', '>', $question->id)->first();

                if (empty($question)) {
                    return redirect('quiz-sets/endquiz/' . $id . '/' . $person->id . '/' . $person->token)->with('flash_message', 'QuizSet End!');
                } else {
                    return redirect('quiz-sets/runquiz/' . $id . '/' . $person->id . '/' . $person->token . '/' . $question->id)->with('flash_message', 'QuizSet updated!');
                }
            }
        }   
        $person->current_question = $question->id;
        $person->update();

        return view('quiz-sets.runquiz', compact('quizset', 'question', 'person', 'token'));
    }

    public function runquizAction(Request $request, $id,$person_id,$token,$question_id){
        $requestData = $request->all();

        $question = Question::findOrFail($question_id);
        $person = Person::where('id', $person_id)->where('token', $token)->first();

        $answerdata = array();
        $answerdata['people_id'] = $person->id;
        $answerdata['question_id'] = $question_id;
        $answerdata['result'] = 0;

        if($question->question_type_id == 1){

            if(isset($requestData['answer'])){
                $answerdata['choice_id'] = $requestData['answer'];
            }else{
                 $answerdata['choice_id'] = 0;
            }
            
            $answerdata['result'] = 0;
            foreach ($question->choices as $choice) {
                if($answerdata['choice_id'] == $choice->id){
                    $answerdata['result'] = $choice->result;
                }
            }

        } elseif ($question->question_type_id == 2){

            $answerdata['choice_id'] = $requestData['answer_id'];
            $answerdata['result_txt'] = $requestData['answertxt'];

        } elseif ($question->question_type_id == 3) {
            $answerdata['choice_id'] = $requestData['answer_id'];
            $answerdata['result_txt'] = $requestData['answertxt'];
            foreach ($question->choices as $choice) {
                if ($requestData['answer_id'] == $choice->id) {
                    if(trim($requestData['answertxt']) == trim($choice->desc))
                    $answerdata['result'] = $choice->result;
                }
            }
        }

        Answer::create($answerdata);

        $quizset = QuizSet::findOrFail($id);
        if (empty($quizset)) {
            return redirect('quiz-sets/endquiz/' . $id . '/' . $person->id .'/'. $person->token)->with('flash_message', 'QuizSet Empty!');
        }else{
            //Next question

            $question =  $quizset->questions()->where('id', '>', $question_id)->first();

            if (empty($question)) {
                return redirect('quiz-sets/endquiz/' . $id . '/' . $person->id . '/' . $person->token)->with('flash_message', 'QuizSet End!');
            } else {
                return redirect('quiz-sets/runquiz/' . $id . '/' . $person->id . '/' . $person->token . '/' . $question_id)->with('flash_message', 'QuizSet updated!');
            }
        }


    }   

    public function endquiz($id, $person_id,$token){

        $person = Person::where('id', $person_id)->where('token', $token)->first();

        $person->status = 'End';
        $person->update();

        return view('quiz-sets.endquiz');
    }

    public function preparequiz($id, $person_id,$token){

        $quizset = QuizSet::findOrFail($id);
        $person = Person::where('id',$person_id)->where('token',$token)->first();

        $person->status = 'Start';
        $person->update();

        $question = array();
        if ($quizset->questions->count() > 0) {
            $question =  $quizset->questions[0];
        }


        return view('quiz-sets.preparequiz', compact('quizset', 'person', 'question', 'token'));
    }
}
