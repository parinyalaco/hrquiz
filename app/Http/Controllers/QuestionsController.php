<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Question;
use App\Models\QuizSet;
use App\Models\QuestionType;
use App\Models\Choice;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $questions = Question::latest()->paginate($perPage);
        } else {
            $questions = Question::latest()->paginate($perPage);
        }

        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $quizsetlist = QuizSet::pluck('name','id');
        $quizsetlist->prepend('===เลือก===','');
        
        $questiontypelist = QuestionType::pluck('name', 'id');
        $questiontypelist->prepend('===เลือก===', '');
        return view('questions.create',compact('quizsetlist', 'questiontypelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        


        if ($request->hasFile('question_image')) {
            $image = $request->file('question_image');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/question/');
            $image->move($destinationPath, $name);

            $requestData['image'] = 'images/question/' . $name;
        }

        $addnewquestion = Question::create($requestData);

        if($addnewquestion->question_type_id == 1){
            $seq = 1; 
            for ($i=1; $i < 7; $i++) { 
                $tmpChoice = array();
                
                if(!empty($requestData['choice_title'.$i])){
                    $tmpChoice['seq'] = $seq;
                    $tmpChoice['question_id'] = $addnewquestion->id;
                    $tmpChoice['title'] = $requestData['choice_title'.$i];
                    $tmpChoice['desc'] = '';
                    $tmpChoice['result'] = $requestData['choice_result' . $i];


                    if ($request->hasFile('choice_image' . $i)) {
                        $image = $request->file('choice_image' . $i);
                        $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path('images/choice/');
                        $image->move($destinationPath, $name);

                        $tmpChoice['image'] = 'images/choice/' . $name;
                    }


                    $tmpChoice['status'] = 'Active';
                    $seq++ ;
                    Choice::create($tmpChoice);
                 }   
            }
        }elseif($addnewquestion->question_type_id == 2){
            $tmpChoice = array();
            $tmpChoice['seq'] = 1;
            $tmpChoice['question_id'] = $addnewquestion->id;
            $tmpChoice['title'] = 'Text';
            $tmpChoice['desc'] = $requestData['choice_desc'];
            $tmpChoice['result'] = '5';
            $tmpChoice['status'] = 'Active';
            Choice::create($tmpChoice);
        } elseif ($addnewquestion->question_type_id == 3) {
            $tmpChoice = array();
            $tmpChoice['seq'] = 1;
            $tmpChoice['question_id'] = $addnewquestion->id;
            $tmpChoice['title'] = 'Match';
            $tmpChoice['desc'] = $requestData['choice_match'];
            $tmpChoice['result'] = '5';
            $tmpChoice['status'] = 'Active';
            Choice::create($tmpChoice);
        }

        return redirect('questions')->with('flash_message', 'Question added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $question = Question::findOrFail($id);

        return view('questions.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);

        $quizsetlist = QuizSet::pluck('name', 'id');
        $quizsetlist->prepend('===เลือก===', '');

        $questiontypelist = QuestionType::pluck('name', 'id');
        $questiontypelist->prepend('===เลือก===', '');

        return view('questions.edit', compact('question', 'quizsetlist', 'questiontypelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();


        if ($request->hasFile('question_image')) {
            $image = $request->file('question_image');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/question/');
            $image->move($destinationPath, $name);

            $requestData['image'] = 'images/question/' . $name;
        }

        $question = Question::findOrFail($id);
        $question->update($requestData);

        if ($question->question_type_id == 1) {
        
        foreach ($question->choices as $choice) {
            //update คำตอบเดิม
            if(isset($requestData['choice_title_id'.$choice->id])){
                $updatechoice = Choice::findOrFail($choice->id);
                $updatechoice->title = $requestData['choice_title_id' . $choice->id];
                $updatechoice->result = $requestData['choice_result_id' . $choice->id];

                if ($request->hasFile('choice_image_id' . $choice->id)) {
                    $image = $request->file('choice_image_id' . $choice->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/choice/');
                    $image->move($destinationPath, $name);

                    $updatechoice->image = 'images/choice/' . $name;
                }

                $updatechoice->update();
            }
            // ลบคำตอบเดิม
            if(isset($requestData['delete_choice' . $choice->id]) && $requestData['delete_choice' . $choice->id] == 'delete'){
                Choice::destroy($choice->id);
            }
        }

        if ($question->question_type_id == 1) {
            //echo "add new 1";
            $seq = 1;
            for ($i = 1; $i < 7; $i++) {
                $tmpChoice = array();

                if (!empty($requestData['choice_title' . $i])) {
                    $tmpChoice['seq'] = $seq;
                    $tmpChoice['question_id'] = $question->id;
                    $tmpChoice['title'] = $requestData['choice_title' . $i];
                    $tmpChoice['desc'] = '';
                    $tmpChoice['result'] = $requestData['choice_result' . $i];

                    if ($request->hasFile('choice_image' . $i)) {
                        $image = $request->file('choice_image' . $i);
                        $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path('images/choice/');
                        $image->move($destinationPath, $name);

                        $tmpChoice['image'] = 'images/choice/' . $name;
                    }

                    $tmpChoice['status'] = 'Active';
                    $seq++;
                    Choice::create($tmpChoice);
                }
            }
        }

        }elseif($question->question_type_id == 2){
            
                foreach ($question->choices as $choice) {
                    //update คำตอบเดิม
                    if (isset($requestData['choice_desc'])) {
                        $updatechoice = Choice::findOrFail($choice->id);
                        $updatechoice->desc = $requestData['choice_desc'];
                        
                        $updatechoice->update();
                    }
                }

        }elseif($question->question_type_id == 3){
                var_dump($requestData);
                foreach ($question->choices as $choice) {
                    //update คำตอบเดิม
                    if (isset($requestData['choice_match'])) {
                        $updatechoice = Choice::findOrFail($choice->id);
                        $updatechoice->desc = $requestData['choice_match'];

                        $updatechoice->update();
                    }
                }
        }
        


        return redirect('questions')->with('flash_message', 'Question updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        foreach ($question->choices as $choice) {
            Choice::destroy($choice->id);
        }


        Question::destroy($id);

        return redirect('questions')->with('flash_message', 'Question deleted!');
    }
}
