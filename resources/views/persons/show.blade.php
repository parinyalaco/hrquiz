@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แบบทดสอบ {{ $person->quizset->name }} ของ {{ $person->name }}</div>
                    <div class="card-body">
                        <button class="btn btn-primary btn-sm" onclick="window.print()"><i class="fa fa-print" aria-hidden="true"></i> Print this page</button>
                        <a href="{{ url('/persons') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/persons/' . $person->id . '/edit') }}" title="Edit Person"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('persons' . '/' . $person->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Person" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Name</th><td>{{ $person->name }}</td> <th>Email</th><td>{{ $person->email }}</td>
                                    </tr>
                                    <tr>
                                       <th>Mobile</th><td>{{ $person->mobile }}</td><th>แบบทดสอบ</th><td>{{ $person->quizset->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ข้อ</th>
                                        <th>ตอบ</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $totalquestion = 0;
                                        $totalanswer = 0;
                                    @endphp
                                    @foreach ($person->quizset->questions as $item)
                                    <tr>
                                        <td>{{ $item->seq }}</td>
                                        <td>
                                            @if ($item->question_type_id == 1)
                                                @if (isset($answers[$item->id]->choice->title))
                                                    {{ $answers[$item->id]->choice->title }}
                                                @else
                                                    -
                                                @endif
                                            @else
                                                @if (isset($answers[$item->id]))
                                                    {{ $answers[$item->id]->result_txt }}
                                                @else
                                                    -
                                                @endif
                                            @endif                                            
                                        </td>
                                        <td>
                                            @if (isset($answers[$item->id]))
                                                    {{ $answers[$item->id]->result }}
                                                @else
                                                    -
                                                @endif
                                                /{{ $item->maxscore }}
                                        @php
                                        $totalquestion += $item->maxscore;
                                        if (isset($answers[$item->id])){
                                            $totalanswer += $answers[$item->id]->result ;
                                        }
                                        @endphp
                                        </td>
                                    </tr>    
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td>{{ $totalanswer }}/{{ $totalquestion }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
