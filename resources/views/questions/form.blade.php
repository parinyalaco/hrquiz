<div class="col-md-4 {{ $errors->has('quiz_set_id') ? 'has-error' : ''}}">
        {!! Form::label('quiz_set_id', 'Set', ['class' => 'control-label']) !!}
        @if (isset($question->quiz_set_id))
            {!! Form::select('quiz_set_id', $quizsetlist,$question->quiz_set_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('quiz_set_id', $quizsetlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('quiz_set_id', '<p class="help-block">:message</p>') !!}

</div>

@if (isset($question->question_type_id))
    <div class="col-md-4 {{ $errors->has('question_type_id') ? 'has-error' : ''}}">
        {!! Form::label('question_type_id', 'แบบ', ['class' => 'control-label']) !!}
        {{ $question->questiontype->name }}
    </div>
@else

<div class="col-md-4 {{ $errors->has('question_type_id') ? 'has-error' : ''}}">
        {!! Form::label('question_type_id', 'แบบ', ['class' => 'control-label']) !!}
        @if (isset($question->question_type_id))
            {!! Form::select('question_type_id', $questiontypelist,$question->question_type_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('question_type_id', $questiontypelist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('question_type_id', '<p class="help-block">:message</p>') !!}
</div>
@endif

<div class="col-md-2 {{ $errors->has('duration') ? 'has-error' : ''}}">
    <label for="duration" class="control-label">{{ 'เวลา(วินาที)' }}</label>
    <input class="form-control" name="duration" type="number" id="duration" value="{{ $question->duration ?? '60'}}" >
    {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-2 {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'สถานะ' }}</label>
    <input class="form-control" name="status" type="text" id="status" value="{{ $question->status ?? 'Active'}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-1 {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'ข้อที่' }}</label>
    <input class="form-control" name="seq" type="number" id="seq" value="{{ $question->seq ?? '1'}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-11 {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ $question->title ?? ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12 {{ $errors->has('question_image') ? 'has-error' : ''}}">
<label for="question_image" class="control-label">{{ 'ภาพโจทย์' }}</label>
    {!! Form::file('question_image', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
@if (isset($question->image))
            <a href="{{ url($question->image) }}" target="_blank"><img height="50px" src="{{ url($question->image) }}" ></a>            
        @endif 
{!! $errors->first('question_image', '<p class="help-block">:message</p>') !!}
</div> 

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'โจทย์' }}</label>
@if (isset($question->desc))
    {!! Form::textarea('desc', $question->desc, ['class'=>'form-control']) !!}
@else
    {!! Form::textarea('desc', '', ['class'=>'form-control']) !!}
@endif
    
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

    <div id='select-1' class="setchoice"
    @if (isset($question->question_type_id) && ($question->question_type_id == 1))
         style="display: block;"
    @else
         style="display: none;"
    @endif
    >
    @if (isset($question->choices) && isset($question->question_type_id) && ($question->question_type_id == 1)) 
        @foreach ($question->choices as $choice)
        <div class='row'>
            <div class="col-md-4 {{ $errors->has('choice_title_id'.$choice->id) ? 'has-error' : ''}}">
        <label for="choice_title_id{{$choice->id}}" class="control-label">{{ 'คำตอบ' }}</label>
        <input type="checkbox" id="delete_choice{{$choice->id}}" name="delete_choice{{$choice->id}}" value="delete">ลบ
        <input class="form-control" name="choice_title_id{{$choice->id}}" type="text" id="choice_title_id{{$choice->id}}" value="{{$choice->title}}">
        {!! $errors->first('choice_title_id'.$choice->id, '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-4 {{ $errors->has('choice_image_id'.$choice->id) ? 'has-error' : ''}}">
        <label for="choice_image_id{{ $choice->id }}" class="control-label">{{ 'Image' }}</label>
           {!! Form::file('choice_image_id'.$choice->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
    @if (isset($choice->image))
            <a href="{{ url($choice->image) }}" target="_blank"><img height="50px" src="{{ url($choice->image) }}" ></a>            
        @endif 
        {!! $errors->first('choice_result'.$choice->id, '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-4 {{ $errors->has('choice_result_id'.$choice->id) ? 'has-error' : ''}}">
        <label for="choice_result_id{{$choice->id}}" class="control-label">{{ 'คะแนน' }}</label>
        <input class="form-control" name="choice_result_id{{$choice->id}}" type="number" id="choice_result_id{{$choice->id}}"  value="{{$choice->result}}">
        {!! $errors->first('choice_result_id'.$choice->id, '<p class="help-block">:message</p>') !!}
        </div>
        </div>
        @endforeach 
    @endif 

        @for ($i = 1; $i < 7; $i++)

        <div class="col-md-4 {{ $errors->has('choice_title'.$i) ? 'has-error' : ''}}">
        <label for="choice_title{{ $i }}" class="control-label">{{ 'คำตอบ'.$i }}</label>
        <input class="form-control" name="choice_title{{ $i }}" type="text" id="choice_title{{ $i }}" >
        {!! $errors->first('choice_title'.$i, '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-4 {{ $errors->has('choice_image'.$i) ? 'has-error' : ''}}">
        <label for="choice_image{{ $i }}" class="control-label">{{ 'Image'.$i }}</label>
           {!! Form::file('choice_image'.$i, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
    
        {!! $errors->first('choice_image'.$i, '<p class="help-block">:message</p>') !!}
        </div> 

        <div class="col-md-4 {{ $errors->has('choice_result'.$i) ? 'has-error' : ''}}">
        <label for="choice_result{{ $i }}" class="control-label">{{ 'คะแนน'.$i }}</label>
        <input class="form-control" name="choice_result{{ $i }}" type="number" id="choice_result{{ $i }}" >
        {!! $errors->first('choice_result'.$i, '<p class="help-block">:message</p>') !!}
        </div>    
        @endfor
        
    </div>    
    <div id='select-2' class="setchoice"
    @if (isset($question->question_type_id) && ($question->question_type_id == 2))
         style="display: block;"
    @else
         style="display: none;"
    @endif
    >

    @if (isset($question->choices) && isset($question->question_type_id) && ($question->question_type_id == 2)) 
        @foreach ($question->choices as $choice)

        <div class="col-md-12 {{ $errors->has('choice_desc') ? 'has-error' : ''}}">
        <label for="choice_desc" class="control-label">{{ 'คำตอบ บรรยาย' }}</label>
        {!! Form::textarea('choice_desc',$choice->desc, ['class'=>'form-control']) !!}
        {!! $errors->first('choice_desc', '<p class="help-block">:message</p>') !!}
        </div>
        @endforeach         
    
        @else

        <div class="col-md-12 {{ $errors->has('choice_desc') ? 'has-error' : ''}}">
        <label for="choice_desc" class="control-label">{{ 'คำตอบ บรรยาย ' }}</label>
        {!! Form::textarea('choice_desc','', ['class'=>'form-control']) !!}
        {!! $errors->first('choice_desc', '<p class="help-block">:message</p>') !!}
        </div>

        @endif 
    </div>  
    <div id='select-3' class="setchoice"
    @if (isset($question->question_type_id) && ($question->question_type_id == 3))
         style="display: block;"
    @else
         style="display: none;"
    @endif
    >
    @if (isset($question->choices) && isset($question->question_type_id) && ($question->question_type_id == 3)) 
        @foreach ($question->choices as $choice)

        <div class="col-md-12 {{ $errors->has('choice_match') ? 'has-error' : ''}}">
        <label for="choice_match" class="control-label">{{ 'คำตอบ ตรงตัว เช่น ผลตัวเลข' }}</label>
        {!! Form::textarea('choice_match',$choice->desc, ['class'=>'form-control']) !!}
        {!! $errors->first('choice_match', '<p class="help-block">:message</p>') !!}
        </div>
        @endforeach         
    
        @else

    <div class="col-md-12 {{ $errors->has('choice_match') ? 'has-error' : ''}}">
        <label for="choice_match" class="control-label">{{ 'คำตอบ ตรงตัว เช่น ผลตัวเลข' }}</label>
        {!! Form::textarea('choice_match',null, ['class'=>'form-control']) !!}
        {!! $errors->first('choice_match', '<p class="help-block">:message</p>') !!}
        </div>
    </div>    
     @endif


<div class="col-md-12">
    <br>
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
