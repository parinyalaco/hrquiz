@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Questions - Import</div>
                    <div class="card-body">
                        <a href="{{ url('/question-types') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        {{-- <form method="GET" action="{{ url('/questions') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form> --}}

                        <br/>
                        <br/>
                        <div class="row g-4 settings-section">
	                
                            <div class="col-12 col-md-12">
                                <div class="app-card app-card-settings shadow-sm p-4">
                                    
                                    <div class="app-card-body">
                                        <form action="{{ route('quiz_import',$id) }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_upload" class="custom-file-input" id="file_upload">
                                                </div>
                                            
                                                <br>
                                                <button class="btn btn-success">Import</button>
                                                {{-- <a class="btn btn-warning" href="{{ route('quiz_export') }}">Export</a> --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('file/'.$id.'.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                    
                                </div><!--//app-card-->
                            </div>
                        </div><!--//row-->

                        {{-- <hr class="my-4">
                        <div class="row g-4 settings-section">
                            <h1 class="app-page-title">ตัวอย่างสำหรับการ Import</h1>
                            <img src="{{asset('/pic/quiz.jpg')}}"  class="photo" width="100" height="250" data-toggle="modal" data-target="#exampleModal">
                        </div><!--//row-->  --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection