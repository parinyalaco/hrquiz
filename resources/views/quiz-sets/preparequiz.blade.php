@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>แบบทดสอบ {{ $quizset->name }}</h3></div>
                    <div class="card-body">
                        {!! nl2br($quizset->desc) !!}<br/><h4>ข้อมูลผู้ทดสอบ</h4><br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ชื่อ-สกุล</th>
                                        <th>Email</th>
                                        <th>มือถือ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $person->name }}</td>
                                        <td>{{ $person->email }}</td>
                                        <td>{{ $person->mobile }}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>

                        
                        
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                         <a href="{{ url('/quiz-sets/runquiz/' . $quizset->id . '/' . $person->id  .'/' . $token.'/'.$question->id) }}" title="Edit QuizSet"><button class="btn btn-primary btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> เริ่มทำข้อสอบ</button></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
