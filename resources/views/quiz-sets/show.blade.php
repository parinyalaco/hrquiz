@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">QuizSet {{ $quizset->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/quiz-sets') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/quiz-sets/' . $quizset->id . '/edit') }}" title="Edit QuizSet"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('quizsets' . '/' . $quizset->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete QuizSet" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="row">
                            <div class="col-md-4"> ID : {{ $quizset->id }}

                            </div>\
                            <div class="col-md-4"> Name : {{ $quizset->name }}

                            </div>
                            <div class="col-md-4"> Status : {{ $quizset->status }}

                            </div>
                        </div>
                        <hr>
                        @foreach ($quizset->questions as $question)
                        <div class="row">
                            <div class="col-md-12">ข้อ : {{ $question->seq }} / แบบ {{ $question->questiontype->name }}</div>
                            <div class="col-md-12">
                                @if (!empty($question->image))
                                                <img width="600px" src="{{ url($question->image) }}" ><br/>
                                            @endif
                                {!! nl2br($question->desc) !!}</div>
                            
                            @if ($question->question_type_id == 1 )
                                @foreach ($question->choices as $item)
                                    <div class="col-md-6">{{ $item->title }} ({{ $item->result }})</div>
                                @endforeach
                            @else
                                @foreach ($question->choices as $item)
                                    <div class="col-md-6">{{ $item->desc }} ({{ $item->result }})</div>
                                @endforeach
                            @endif
                            
                            
                        </div>    
                        <br/>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
