@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>แบบทดสอบ {{ $quizset->name }}</h3></div>
                    <div class="card-body">
                        {!! nl2br($quizset->desc) !!}
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/quiz-sets/startquizAction/'.$quizset->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {!! Form::hidden('quiz_set_id',  $quizset->id) !!}
                            <div class="row">
                                <div class="col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
                                    <label for="name" class="control-label">{{ 'ชื่อ สกุล' }}</label>
                                    <input class="form-control" name="name" type="name" id="title" required value="" >
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-6 {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <label for="email" class="control-label">{{ 'Email' }}</label>
                                    <input class="form-control" name="email" type="email" id="email" required value="" >
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-6 {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                    <label for="mobile" class="control-label">{{ 'มือถือ' }}</label>
                                    <input class="form-control" name="mobile" type="text" id="mobile" required value="" >
                                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-6"><br/>
                                    <input class="btn btn-primary" type="submit" value="บันทึกข้อมูลผู้สอบ">
                                </div>
                            </div>
                            
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
