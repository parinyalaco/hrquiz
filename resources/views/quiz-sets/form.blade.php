<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อ Quiz Set' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $quizset->name ?? ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
@if (isset($quizset->desc))
    {!! Form::textarea('desc', $quizset->desc, ['class'=>'form-control']) !!}
@else
    {!! Form::textarea('desc', '', ['class'=>'form-control']) !!}
@endif
    
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="Status" class="control-label">{{ 'Status' }}</label>
    <input class="form-control" name="status" type="text" id="status" value="{{ $quizset->status ?? ''}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
@if($quizset->owner_id==Auth::user()->id || Auth::user()->group=='Admin')
    <div class="form-group {{ $errors->has('owner_id') ? 'has-error' : ''}}">
        <label for="owner_id" class="control-label">{{ 'owner_id' }}</label>
        {{ Form::select('owner_id', $user_pluck , $quizset->owner_id , [
										'placeholder' => 'ไม่ระบุ', 
										'class' => 'form-select'
										]) }}
        {{-- <input class="form-control" name="owner_id" type="text" id="owner_id" value="{{ $quizset->owner_id ?? ''}}" > --}}
        {!! $errors->first('owner_id', '<p class="help-block">:message</p>') !!}
    </div>
@endif


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
