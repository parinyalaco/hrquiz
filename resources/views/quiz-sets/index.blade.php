@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Quizsets</div>
                    <div class="card-body">
                        <a href="{{ url('/quiz-sets/create') }}" class="btn btn-success btn-sm" title="Add New QuizSet">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/quiz-sets') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ชื่อ Quiz Set</th>
                                        <th>จำนวนข้อสอบ</th>
                                        <th>Status</th>
                                        <th>Owner</th>
                                        {{-- <th>ผู้จัดทำ</th> --}}
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($quizsets as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->questions->count() }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>{{ $item->user_name }}</td>
                                        {{-- <td>{{ $item->status }}</td> --}}
                                        <td>
                                            <a href="{{ url('/quiz-sets/startquiz/' . $item->id) }}" title="Start QuizSet"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> สร้าง Link ทดสอบ</button></a>                                           
                                            <a href="{{ url('/quiz-sets/' . $item->id) }}" title="View QuizSet"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            @if($item->owner_id==Auth::user()->id || Auth::user()->group=='Admin')
                                                <a href="{{ url('/quiz-sets/' . $item->id . '/edit') }}" title="Edit QuizSet"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                <form method="POST" action="{{ url('/quiz-sets' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete QuizSet" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $quizsets->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
