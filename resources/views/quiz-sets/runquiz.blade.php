@extends('layouts.runquiz')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>แบบทดสอบ {{ $quizset->name }} / {{ $person->name }}</h3></div>
                    <div class="card-body">
                        {!! nl2br($quizset->desc) !!}

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" id='questionrun' action="{{ url('/quiz-sets/runquizAction/'.$quizset->id.'/'.$person->id.'/'.$token.'/'.$question->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12"><h4>ข้อ : {{ $question->seq }}</h4></div>
                                        <div class="col-md-12">{!! nl2br($question->desc) !!}<br>
                                        @if (!empty($question->image))
                                            <img width="600px" src="{{ url($question->image) }}" >
                                            
                                        @endif
                                        
                                        </div>
                                        
                                        @if ($question->questiontype->id == 1) 
                                        
                                        @foreach ($question->choices as $item)
                                        <div class="col-md-6">
                                            {!! Form::radio('answer', $item->id) !!}
                                            @if (empty($item->image))
                                                {{ $item->title }} 
                                            @else
                                                <img width="200px" src="{{ url($item->image) }}" >
                                            @endif
                                            
                                        
                                        </div>
                                        @endforeach

                                        @else

                                        @if ($question->questiontype->id == 2) 
                                        
                                        @foreach ($question->choices as $item)
                                        <div class="col-md-12">
                                            {!! Form::hidden('answer_id',  $item->id) !!}
                                            {!! Form::textarea('answertxt', '', ['class'=>'form-control']) !!}
                                        </div>
                                        @endforeach

                                        @else
                                        
                                        @if ($question->questiontype->id == 3) 
                                        
                                        @foreach ($question->choices as $item)
                                        <div class="col-md-12">
                                            {!! Form::hidden('answer_id',  $item->id) !!}
                                            {!! Form::text('answertxt', '', ['class'=>'form-control']) !!}
                                        </div>
                                        @endforeach

                                        @else
                                            
                                        @endif

                                        @endif
                                            
                                        @endif

                                        
                                        <div class="col-md-12"><br/>
                                        <input class="btn btn-primary" type="submit" value="ส่งคำตอบ">
                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-3"><h2><span class="c" id="{{ $question->duration }}"></span></h2> </div>
                            </div>
                            
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
