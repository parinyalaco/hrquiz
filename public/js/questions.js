$(document).ready(function () {
    $('#question_type_id').change(function(){

        $(".setchoice").hide();
        $("#select-" + $(this).val()).show();

    })
});

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    alert("Copied the text: " + $(element).text());
}
