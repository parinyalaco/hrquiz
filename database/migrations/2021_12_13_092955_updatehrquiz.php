<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Updatehrquiz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'quiz_sets',
            function (Blueprint $table) {
                $table->integer('owner_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('quiz_sets', 'owner_id')) {
            Schema::table('quiz_sets', function (Blueprint $table) {
                $table->dropColumn('owner_id');
            });
        }
    }
}
